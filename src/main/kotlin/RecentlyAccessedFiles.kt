import java.util.*

/**
 * Created by Lucas Piscello
 */
class RecentlyAccessedFiles {

    var enabled: Boolean = true

    private var history: LinkedList<String> = LinkedList()

    fun getHistory() : LinkedList<String> {
        return history
    }

    fun accessFile(file: String) {
        if (!enabled) return

        recordAccess(file)
        limitHistory()
    }

    private fun recordAccess(file: String) {
        history.remove(file)
        history.addFirst(file)
    }

    private fun limitHistory() {
        if (history.size > 15)
            history.removeLast()
    }

    fun clear() {
        history.clear()
    }

}