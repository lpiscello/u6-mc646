import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Created by Lucas Piscello
 */
class RecentlyAccessedFilesTest {

    @Test
    fun startupTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        assertEquals(0, recentlyAccessedFiles.getHistory().size, "History should be empty on startup")
    }

    @Test
    fun accessSingleFileTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        recentlyAccessedFiles.accessFile("File 01")

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(1, history.size, "History should have one file")
        assertEquals("File 01", history[0], "History first item should be File 01")
    }

    @Test
    fun accessMultipleFilesTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        recentlyAccessedFiles.accessFile("File 01")
        recentlyAccessedFiles.accessFile("File 02")

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(2, history.size, "History should have two files")
        assertEquals("File 02", history[0], "History first item should be File 02")
        assertEquals("File 01", history[1], "History second item should be File 01")
    }

    @Test
    fun accessSameFileTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        recentlyAccessedFiles.accessFile("File 01")
        recentlyAccessedFiles.accessFile("File 02")
        recentlyAccessedFiles.accessFile("File 01")

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(2, history.size, "History should have two files")
        assertEquals("File 01", history[0], "History first item should be File 01")
        assertEquals("File 02", history[1], "History second item should be File 02")
    }

    @Test
    fun accessSameFileMultipleTimesTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        recentlyAccessedFiles.accessFile("File 01")
        recentlyAccessedFiles.accessFile("File 01")
        recentlyAccessedFiles.accessFile("File 01")
        recentlyAccessedFiles.accessFile("File 01")

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(1, history.size, "History should have one files")
        assertEquals("File 01", history[0], "History first item should be File 01")
    }

    @Test
    fun historyLimitTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        (1..17).forEach {
            val fileName = "File ${it.toString().padStart(2, '0')}"
            recentlyAccessedFiles.accessFile(fileName)
        }

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(15, history.size, "History should have 15 files")
        assertEquals("File 17", history.first, "History first item should be File 17")
        assertEquals("File 03", history.last, "History last item should be File 03")
    }

    @Test
    fun accessSameFileOnFullHistoryTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        (1..17).forEach {
            val fileName = "File ${it.toString().padStart(2, '0')}"
            recentlyAccessedFiles.accessFile(fileName)
        }

        recentlyAccessedFiles.accessFile("File 15")

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(15, history.size, "History should have 15 files")
        assertEquals("File 15", history.first, "History first item should be File 15")
        assertEquals("File 17", history[1], "History second item should be File 17")
        assertEquals("File 03", history.last, "History last item should be File 03")
    }

    @Test
    fun clearHistoryTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        recentlyAccessedFiles.accessFile("File 01")
        recentlyAccessedFiles.clear()

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(0, history.size, "History should be empty")
    }

    @Test
    fun clearEmptyHistoryTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        recentlyAccessedFiles.clear()

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(0, history.size, "History should be empty")
    }

    @Test
    fun disableHistoryRecordingTest() {
        val recentlyAccessedFiles = RecentlyAccessedFiles()
        recentlyAccessedFiles.accessFile("File 01")
        recentlyAccessedFiles.enabled = false
        recentlyAccessedFiles.accessFile("File 02")
        recentlyAccessedFiles.enabled = true
        recentlyAccessedFiles.accessFile("File 03")

        val history = recentlyAccessedFiles.getHistory()
        assertEquals(2, history.size, "History should have one file")
        assertEquals("File 03", history.first, "History first item should be File 03")
        assertEquals("File 01", history[1], "History second item should be File 01")
    }

}